(function () {
    const classmates = [
        {
            name: 'Іванова Валентина'
        },
        {
            name: 'Човник Сергій'
        },
        {
            name: 'Ага Тетяна'
        }
    ];

    const ulWrapper = customDOM.getById('classmateWrapper');

    setTimeout(function () {

        classmates.forEach(function (person) {
            const li = customDOM.createElement('li');

            li.innerHTML = person.name;

            customDOM.append(ulWrapper, li);

        });


    }, 2000);


})();
