/**
 * @name customDOM
 * @desc Об'єкт який реалізує логіку роботи з DOM-ом
 * @type {Object}
 **/
const customDOM = {

        /**
         * @name getAllElements
         * @return {Collection|Array}
         **/
        getAllElements: function () {
            return document.getElementsByTagName('*');
        },

        /**
         * @desc Елемент по ідентифікатору
         * @return {Element}
         **/
        getById: function(id) {
            return document.getElementById(id);
        },

        /**
         * @desc Створення елементу
         * @return {Object}
         **/
        createElement: function (tagName) {
            return document.createElement(tagName);
        },

        /**
         * @desc Додаємо child-елемент до parent
         * @param {Object} parent
         * @param {Object} child
         * @return {Object}
         **/
        append: function (parent, child) {
            return parent.appendChild(child);
        }

    }
;