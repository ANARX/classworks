const user = {
    name: 'Serhii',
    rate: 35,
    coef: 12000,
    departments: [
        {
            title: 'IT',
            code: 'it-234'
        }
    ]
};

for(let key in user) {
    console.log(`${key} === > ${user[  key ]}`);
}