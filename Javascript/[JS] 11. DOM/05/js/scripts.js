(function () {
    const button = document.querySelector('#SubmitMe');

    if (!button) {
        return;
    }

    setTimeout(() => {
        button.removeAttribute('disabled');


        setTimeout(() => {
            button.setAttribute('disabled', 'disabled');
        }, 2000);
    }, 2500);

})();


