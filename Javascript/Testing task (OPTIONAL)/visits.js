// Напишите функцию, которая будет вычислять процентное соотношение посещений заведения на протеяжении определенного периода
// Функция принимает id того места, которое нужно измерить (category_id), дату начала сравнения (date_from) и дату конца (date_to) не включитально (в формате ГГГГ-ММ-ДД)
// После этого функция должна вернуть объект со следующими полями:
// - category_id
// - category_name
// - start_data
// - array_of_percentage_days
// array_of_percentage_days - должен быть массивом, длинна которого должна развнять количеству дней в промежутке от входящих параметров (date_from и date_to)
// каждое значение в данном массиве - процентное соотношение посещений данного заведения по данному периоду
// пример - function(2, '2019-02-02', '2019-02-04') => должен вернуть
//  {
//   category_id: 2
//   category_name: 'SomeName',
//   start_data: '2019-02-02',
//   array_of_percentage_days: [45, 55] // так как всего два дня в текущем промежутке
//  }
// массив элементов для поиска - снизу в коде

// Если функция не находит заведение по ID - возвращает ошибку


const records = [
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Tue, 14 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Fri, 17 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 12 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 40,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 13 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 11 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 15 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 15 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 5,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 11 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 40,
    category_name: 'McDonald\'s',
    category_timeslot: 'Fri, 17 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 17,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 18 Jan 2020 8:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 2,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 18 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 22,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 18 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 18 Jan 2020 16:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Tue, 21 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Thu, 23 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 8,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 19 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 4,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 19 Jan 2020 06:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 2,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 1,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 18,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 19 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 20 Jan 2020 09:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 17,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 18:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 0,
    category_name: 'McDonald\'s',
    category_timeslot: 'Fri, 24 Jan 2020 17:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 4,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 8,
    category_name: 'McDonald\'s',
    category_timeslot: 'Thu, 23 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 5,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Tue, 21 Jan 2020 16:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 13 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 5,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 12 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 2,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 12 Jan 2020 06:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 4,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 11 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sat, 11 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 2,
    category_name: 'McDonald\'s',
    category_timeslot: 'Fri, 10 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 0,
    category_name: 'McDonald\'s',
    category_timeslot: 'Fri, 10 Jan 2020 04:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 5,
    category_name: 'McDonald\'s',
    category_timeslot: 'Thu, 09 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 40,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 08 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 17,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 08 Jan 2020 8:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 2,
    category_name: 'McDonald\'s',
    category_timeslot: 'Tue, 07 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Tue, 07 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 10,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 06 Jan 2020 16:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 1,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 06 Jan 2020 14:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 4,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 06 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Thu, 23 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 8,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 19 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 19 Jan 2020 16:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 4,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 19:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 12,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 19:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 11,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 18:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 18,
    category_name: 'McDonald\'s',
    category_timeslot: 'Sun, 19 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Mon, 20 Jan 2020 09:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 17,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 18:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 20,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 0,
    category_name: 'McDonald\'s',
    category_timeslot: 'Fri, 24 Jan 2020 17:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 4,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 08:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 8,
    category_name: 'McDonald\'s',
    category_timeslot: 'Thu, 23 Jan 2020 10:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 5,
    category_name: 'McDonald\'s',
    category_timeslot: 'Wed, 22 Jan 2020 12:00 GMT',
  },
  {
    category_id: 1,
    category_FCV: 14,
    category_name: 'McDonald\'s',
    category_timeslot: 'Tue, 21 Jan 2020 16:00 GMT',
  }
]
