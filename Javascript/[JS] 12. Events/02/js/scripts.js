document.addEventListener('DOMContentLoaded', onReadyHandler);

/**
 *
 **/
function onReadyHandler() {
    const links = document.getElementsByTagName('a');

    for(let element of links) {
        element.addEventListener('click', linkHandler  );
    }

    // links.onclick = function(event) {
    //     event.preventDefault();
    //
    //
    // }
}

/**
 *
 **/
function linkHandler(e) {
    if (!confirm('Are you sure?')) {
        e.preventDefault();
    }
}


function onFormSubmit(event) {
    event.preventDefault();

    const form = event.target;
    const data = {};

    data.hello = 'dffg';

    for(let element of form) {
        if (element.type === 'text') {
            data [element.name] = element.value;
        }
    }

    console.log('Data --> ', data);

}


function onInputFocus(e) {
    const element = e.target;

    element.style.borderColor = 'red';
}

function onInputBlur(e) {
    const element = e.target;

    element.style.borderColor = '';
}