document.addEventListener('DOMContentLoaded', function () {
    const btn = document.getElementById('sendMessage');

    if (!btn) {
        console.error('Element with ID "sendMessage" was not found!');
        return;
    }

    btn.setAttribute('disabled', 'disabled');
    btn.addEventListener('click', onSendButtonClick);
    btn.removeAttribute('disabled');
});


function onSendButtonClick() {
    const firstName = document.getElementById('firstName');
    const phone = document.getElementById('phone');

    if (!firstName || !phone) {
        console.error('Element with ID "firstName" or "phone" was not found!');
        return;
    }

    const data = {
        firstName:  firstName.value,
        phone:  phone.value,
    };

    console.log(data);

}