document.addEventListener('DOMContentLoaded', onDocumentLoaded);

/**
 * @desc
 **/
function onDocumentLoaded() {
    const btn = document.createElement('button');
    const btnSend = document.getElementById('sendMessage');

    btn.setAttribute('type', 'button');
    btn.setAttribute('id', 'labMouse');
    btn.innerHTML = 'Лабораторная мышка';
    btn.onclick = function() {
        console.log('Dynamic event!!!!!');
    };

    btnSend.parentNode.insertBefore(btn, btnSend);

    console.log('Tests!!!!!');
}

/**
 * @desc Что то там делает
 **/
function onClientFormBtnClick() {
    const btnMouse = document.getElementById('labMouse');

    btnMouse.addEventListener('click', function() {
        console.log('Покажи мне дату -> ', Date.now());
    });



    // alert('Hello');
}