document.addEventListener('DOMContentLoaded', onDocumentReady);

const fieldWidth = 540;
const fieldHeight = 350;

const ball = document.createElement('img');
const field = document.getElementById('field-wrapper');

let leftPosition = 0;
let topPosition = 0;

ball.style.position = 'absolute';
ball.style.width = '50px';
ball.style.height = '50px';

ball.setAttribute('src', './img/ball.png');

document.onkeydown = function(event) {

        if (leftPosition <= 10 && topPosition > 130) {

            alert('Гол!!!!!');
            setTimeout(function() {
                initBall();
            }, 300);

            return false;
        }


        if (event.code === 'ArrowRight') {
            ball.style.left = `${leftPosition.toFixed()}px`;
            leftPosition += 10;
        }
        else if (event.code === 'ArrowLeft') {
            ball.style.left = `${leftPosition.toFixed()}px`;
            leftPosition -= 10;
        }
        else if (event.code === 'ArrowUp') {
            ball.style.top = `${leftPosition.toFixed()}px`;
            leftPosition -= 10;
        }
        else if (event.code === 'ArrowDown') {
            ball.style.top = `${leftPosition.toFixed()}px`;
            leftPosition += 10;
        }
};


function onDocumentReady() {
    field.appendChild(ball);

    setTimeout(function() {
        initBall();
    }, 500);

}

function startAgain() {
    initBall();
}

/**
 * @desc Мяч по центру
 * */
function initBall() {
    leftPosition = fieldWidth / 2;
    topPosition = (fieldWidth / 2) - 50;

    ball.style.top = `${topPosition.toFixed()}px`;
    ball.style.left = `${leftPosition.toFixed()}px`;
}

// document.onkeyup = function(event) {
//     console.log('Event Key Down -> ', event);
// };