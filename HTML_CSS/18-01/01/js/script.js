const num1 = +prompt('Введіть перше число!', '0');
const num2 = +prompt('Введіть друге число!', '0');

if (isNaN(num1)) {
    throw new Error('Ймовірно, ви ввели не число!');
}

const sumResult = num1 + num2;
const prodResult = num1 * num2;

alert('Sum -> ' + sumResult + ', Prod -> ' + prodResult);

alert(`Sum -> ${sumResult}, Prod -> ${prodResult}`);
