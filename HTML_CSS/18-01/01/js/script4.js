const monNumber = +prompt('Введіть номер місяця!', '1');
let strMonth = 'січень';


switch(monNumber) {
    case 1:
        strMonth = 'січень';
        break;
    case 2:
        strMonth = 'лютий';
        break;
    case 3:
        strMonth = 'березень';
        break;
    case 4:
        strMonth = 'квітень';
        break;
    case 5:
        strMonth = 'травень';
        break;
    case 6:
        strMonth = 'червень';
        break;
    case 7:
        strMonth = 'липень';
        break;
    case 8:
        strMonth = 'серпень';
        break;
    case 9:
        strMonth = 'вересень';
        break;
    case 10:
        strMonth = 'жовтень';
        break;
    case 11:
        strMonth = 'листопад';
        break;
    case 12:
        strMonth = 'грудень';
        break;

    default:
        alert('Не вірне значення!!!');
        strMonth = 'invalid';

}

console.log(`Числу ${monNumber} відповідає місяць "${strMonth}"`);
