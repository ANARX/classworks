let result = 0;
let input = null;
let strings = '';

do {
    input = prompt('Введіть число!');

    if (input === null) {
        break;
    } else if (isNaN(input)) {
        alert('Дивіться, що вводите!');
        input = true;
    } else if (input >= 0) {
        result += +input;
        strings += `${input},`;
    }
} while (input);

console.log(`Сума ряда ${strings} дорівнює ${result}`);