'use strict';

const str = 'Hello world!!!';

// Q: Описать своими словами в несколько строчек,
// зачем в программировании нужны циклы.
// A:
//
// console.log(x);


// for(let i = 0; i < 10; i++) {
//     console.log(i);
//     // console.log(`Символ під індексом ${i} дорівнює ${str.charAt(i)}`);
// }

function isSimple(end, x){
    let count = 0;

    for (let i = 1; i < end; i++) {
        if (x % i === 0) {
            count++;
        }
    }

    return count === 2;
}