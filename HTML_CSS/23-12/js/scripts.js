const userYearOfBirth = Number(prompt('Введите год рождения Пользователя!'));
const userMonthOfBirth = Number(prompt('Введите месяц рождения Пользователя!'));
const userDayOfBirth = Number(prompt('Введите день рождения Пользователя!'));

const nowYear = 2019;
const nowMonth = 7;
const nowDay = 23;

if (isNaN(userYearOfBirth) || userYearOfBirth === 0) {
    alert('Смотри, что вводишь!');
    throw new Error('Ошибка ввода: не верный формат ввода для Года рождения Пользователя!'); // НЕ ОБРАЩАТЬ ВНИМАНИЕ
}

if (isNaN(userMonthOfBirth) || userMonthOfBirth === 0) {
    alert('Смотри, что вводишь!');
    throw new Error('Ошибка ввода.');// НЕ ОБРАЩАТЬ ВНИМАНИЕ
}

if (isNaN(userDayOfBirth) || userDayOfBirth === 0) {
    alert('Смотри, что вводишь!');
    throw new Error('Ошибка ввода.');// НЕ ОБРАЩАТЬ ВНИМАНИЕ
}

if (userYearOfBirth > nowYear) {
    alert('Год рождения Пользователя должен быть меньше текущего!');
    throw new Error('Ошибка ввода.');// НЕ ОБРАЩАТЬ ВНИМАНИЕ
}

// !!!! Здесь начинается ВЫЧИСЛЕНИЯ --- !!!!!

let resultYear; //для хранения результата по году
let resultMonth; // ...
let resultDay; // ...

if (nowMonth >= userMonthOfBirth && nowDay >= userDayOfBirth) {
    resultYear = nowYear - userYearOfBirth;
    resultMonth = nowMonth - userMonthOfBirth;
    resultDay = nowDay - userDayOfBirth;
}
else if (nowDay >= userDayOfBirth && nowMonth < userMonthOfBirth) {
    resultYear = nowYear - userYearOfBirth - 1;
    resultMonth = nowMonth + 12 - userMonthOfBirth;
    resultDay = nowDay - userDayOfBirth;
}
// ...

console.log(resultYear, resultMonth, resultDay);
