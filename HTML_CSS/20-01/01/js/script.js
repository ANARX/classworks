{
    let result = 0;
    const SIZE = 3;

    for (let i = 0; i < SIZE; i++) {
        for (let j = 0; j < SIZE; j++) {
            if (i === j) {
                result = result + Number(
                    (Math.random() * 100).toFixed()
                );
            }
        }
    }

    console.log(result);
}
